#include "segmentedcar.h"

SegmentedCar::SegmentedCar(AbsCurve* track, int cars) {
    cars = cars > 0? cars: 1;
    int dist = track->getNumberOfSegments()/10;

    for(int i = 0; i < cars; i++) {
        Car* car = new Car(track);
        car->setExtraSpaces(i*dist, (cars - i)*dist);
        this->cars.push_back(car);
    }
}

void SegmentedCar::onDisplay() {
    for(auto c: cars)
        c->display();
}

void SegmentedCar::onDisplaySelected() {
    onDisplay();
}

void SegmentedCar::onDisplayShadow() {
    onDisplay();
}

void SegmentedCar::onKeyCharPressed(int key) {
    for(auto c: cars)
        c->onKeyCharPressed(key);
}

void SegmentedCar::onKeyPressed(int key) {
    for(auto c: cars)
        c->onKeyPressed(key);
}

void SegmentedCar::advance() {
    for(auto c: cars)
        c->advance();
}

void SegmentedCar::goBack() {
    for(auto c: cars)
        c->goBack();
}

Camera *SegmentedCar::getCamera() {
    Car* car = cars[0];
    return car->getCamera();
}

void SegmentedCar::setCamera(Camera *cam) {
    Car* car = cars[0];
    car->setCamera(cam);
}

void SegmentedCar::setCamera2(Camera *cam) {
    Car* car = cars[0];
    car->setCamera2(cam);
}

