#ifndef MATRIXES
#define MATRIXES

#include "matrix.h"

#define MATRIX_CUBIC { {-4.5, 13.5, -13.5, 4.5}, {9, -22.5, 18, -4.5}, {-5.5, 9, -4.5, 1}, {1, 0, 0, 0} }
#define MATRIX_BEZIER { {-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 3, 0, 0}, {1, 0, 0, 0} };
#define MATRIX_B_SPLINE { {-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 0, 3, 0}, {1, 4, 1, 0} };
#define MATRIX_CATMULL_ROM { {-1, 3, -3, 1}, {2, -5, 4, -1}, {-1, 0, 1, 0}, {0, 2, 0, 0} }

#endif // MATRIXES

