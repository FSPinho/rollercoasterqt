#include "viewport.h"

ViewPort::ViewPort() : ViewPort(MODE_PESPECTIVE, POINT_DEFAULT) {

}

ViewPort::ViewPort(int viewMode, int point) {
    this->x = 0;
    this->y = 0;
    this->width = 1;
    this->height = 1;

    viewAngleX = 0.0f;
    viewAngleY = 0.0f;

    defaultCam = new Camera(0, 1, 3, 0, 0, 0, 0, 1, 0);
    defaultCam->setViewMode(viewMode, point);
    clearCams();

    selected = false;
}

void ViewPort::display() {
    /* Viewport divisions */
    glViewport(x, y, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glOrtho(0, width, 0, height, 0, 1000);
    gluLookAt(0, 0, 10, 0, 0, 0, 0, 1, 0);

    glLineWidth(2);
    //glEnable(GL_COLOR_LOGIC_OP);
    //glLogicOp(GL_XOR);

    // Verify if viewport is selected
    if(selected) {
        glColor3d(1, 0, 0);
    } else {
        glColor3d(1, 1, 1);
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glRecti(0, 0, width-1, height-1);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_COLOR_LOGIC_OP);


    /* View port preparation */
    glViewport(x, y, width, height);
    reloadViewParams();
}

void ViewPort::displayToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]) {
    glViewport(x, y, width, height);
    reloadViewParamsToSelect(cursorX, cursorY, w, h, BUFSIZE, selectBuf);
}

void ViewPort::reshape(float scaleW, float scaleH) {
    this->width *= scaleW;
    this->height *= scaleH;
    this->x *= scaleW;
    this->y *= scaleH;
    // Para previnir uma divisão por zero
    if ( height == 0 ) height = 1;

    // Especifica o tamanho da viewport
    glViewport(x, y, width, height);

    reloadViewParams();
}

void ViewPort::reshape(float x, float y, float w, float h) {
    this->width = w;
    this->height = h;
    this->x = x;
    this->y = y;
    // Para previnir uma divisão por zero
    if ( height == 0 ) height = 1;

    // Especifica o tamanho da viewport
    glViewport(x, y, width, height);

    reloadViewParams();
}

void ViewPort::moveCam(float x, float y) {
    float camCX = getAtualCam()->getCenterX();
    float camEX = getAtualCam()->getEyeX();
    float camCY = getAtualCam()->getCenterY();
    float camEY = getAtualCam()->getEyeY();

    camCX += (x)*0.1;
    camEX += (x)*0.1;

    camCY -= (y)*0.1;
    camEY -= (y)*0.1;

    getAtualCam()->setCenterX(camCX);
    getAtualCam()->setCenterY(camCY);
    getAtualCam()->setEyeX(camEX);
    getAtualCam()->setEyeY(camEY);
}

void ViewPort::addCamera(Camera *cam) {
    cams.push_back(cam);
}

void ViewPort::zoom(float zoom) {
    getAtualCam()->zoom(zoom);
}

void ViewPort::setAngles(float h, float v) {
    this->viewAngleX = v;
    this->viewAngleY = h;
}

bool ViewPort::containsClick(int x, int y) {
    return x >= this->x && x <= this->x + width &&
            y >= this->y && y <= this->y + height;
}

bool ViewPort::inBound(float x, float y) {
    return inTop(x, y) || inBottom(x, y) || inLeft(x, y) || inRight(x, y);
}

bool ViewPort::inTop(float x, float y) {
    return x >= this->x - MOUSE_LIMIT && x <= this->x + width + MOUSE_LIMIT &&
            y >= this->y - MOUSE_LIMIT && y < this->y + MOUSE_LIMIT;
}

bool ViewPort::inBottom(float x, float y) {
    return x >= this->x - MOUSE_LIMIT && x <= this->x + width + MOUSE_LIMIT &&
             y >= this->y + height - MOUSE_LIMIT && y < this->y + height + MOUSE_LIMIT;
}

bool ViewPort::inLeft(float x, float y) {
    return y >= this->y - MOUSE_LIMIT && y <= this->y + height + MOUSE_LIMIT &&
            x >= this->x - MOUSE_LIMIT && x < this->x + MOUSE_LIMIT;
}

bool ViewPort::inRight(float x, float y) {
    return y >= this->y - MOUSE_LIMIT && y <= this->y + height + MOUSE_LIMIT &&
            x >= this->x + width - MOUSE_LIMIT && x < this->x + width + MOUSE_LIMIT;
}

bool ViewPort::inTopParalel(float x, float y) {
    return y >= this->y - MOUSE_LIMIT && y < this->y + MOUSE_LIMIT;
}

bool ViewPort::inBottomParalel(float x, float y) {
    return y >= this->y + height - MOUSE_LIMIT && y < this->y + height + MOUSE_LIMIT;
}

bool ViewPort::inLeftParalel(float x, float y) {
    return x >= this->x - MOUSE_LIMIT && x < this->x + MOUSE_LIMIT;
}

bool ViewPort::inRightParalel(float x, float y) {
    return x >= this->x + width - MOUSE_LIMIT && x < this->x + width + MOUSE_LIMIT;
}

void ViewPort::markBound(float x, float y) {
    top = inTopParalel(x, y);
    bottom = inBottomParalel(x, y);
    left = inLeftParalel(x, y);
    right = inRightParalel(x, y);
}

void ViewPort::unmarkBound() {
    top = bottom = left = right = false;
}

bool ViewPort::isMarked() {
    return top || bottom || left || right;
}

void ViewPort::moveBound(float x, float y) {
    if(top) {
        this->y += y;
        height -= y;

    } if (bottom) {
        height += y;

    } if(left) {
        this->x += x;
        width -= x;

    } if(right) {
        width += x;

    }
}

bool ViewPort::isMovableBound(float x, float y) {
    return !((height-y < MOUSE_LIMIT*2 && top) ||\
            (height+y < MOUSE_LIMIT*2 && bottom) ||\
            (width-x < MOUSE_LIMIT*2 && left) ||\
            (width+x < MOUSE_LIMIT*2 && right));
}

float ViewPort::getX() const {
    return x;
}

void ViewPort::setX(float value) {
    x = value;
}

float ViewPort::getY() const {
    return y;
}

void ViewPort::setY(float value) {
    y = value;
}

float ViewPort::getWidth() const {
    return width;
}

void ViewPort::setWidth(float value) {
    width = value;
}

float ViewPort::getHeight() const {
    return height;
}

void ViewPort::setHeight(float value) {
    height = value;
}

void ViewPort::reloadViewParams() {
    Camera* cam = getAtualCam();

    // Especifica sistema de coordenadas de projeção
    glMatrixMode(GL_PROJECTION);
    // Inicializa sistema de coordenadas de projeção
    glLoadIdentity();

    cam->setAspect((float)width/height);
    cam->updateViewMode();

    // Especifica sistema de coordenadas do modelo
    glMatrixMode(GL_MODELVIEW);
    // Inicializa sistema de coordenadas do modelo
    glLoadIdentity();

    cam->rotateVertical(viewAngleX);
    cam->rotateHorizontal(viewAngleY);

    // Especifica posição do observador e do alvo
    gluLookAt(cam->getEyeX(), cam->getEyeY(), cam->getEyeZ(), \
              cam->getCenterX(), cam->getCenterY(), cam->getCenterZ(), \
              cam->getUpX(), cam->getUpY(), cam->getUpZ());
    viewAngleX = viewAngleY = 0.0f;
}

void ViewPort::reloadViewParamsToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]) {
    glDisable(GL_LIGHTING);

    GLint viewport[4];

    Camera* cam = getAtualCam();

    glSelectBuffer(BUFSIZE, selectBuf);
    glRenderMode(GL_SELECT);

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    cam->setAspect((float)width/height);
    cam->updateViewMode();

    glGetIntegerv(GL_VIEWPORT,viewport);
    gluPickMatrix(cursorX, viewport[3] - cursorY, w, h, viewport);

    glMatrixMode(GL_MODELVIEW);
    glInitNames();
    glLoadIdentity();

    cam->rotateVertical(viewAngleX);
    cam->rotateHorizontal(viewAngleY);

    // Especifica posição do observador e do alvo
    gluLookAt(cam->getEyeX(), cam->getEyeY(), cam->getEyeZ(), \
              cam->getCenterX(), cam->getCenterY(), cam->getCenterZ(), \
              cam->getUpX(), cam->getUpY(), cam->getUpZ());

    glEnable(GL_LIGHTING);
}

Camera *ViewPort::getAtualCam() {
    return cams.at(camera);
}

void ViewPort::clearCams() {
    cams.clear();
    cams.push_back(defaultCam);
    camera = 0;
}

bool ViewPort::getSelected() const {
    return selected;
}

void ViewPort::setSelected(bool value) {
    selected = value;
}

void ViewPort::nextCamera() {
    ++camera %= cams.size();
    reloadViewParams();
}
