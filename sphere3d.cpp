#include "sphere3d.h"

Sphere3D::Sphere3D(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {}

Sphere3D::~Sphere3D() {

}

void Sphere3D::onDisplay() {
    glColor3f(0, 0, 1);
    glutSolidSphere(.2, 16, 16);
}

void Sphere3D::onDisplaySelected() {
    glColor3f(1, 0, 0);
    glutSolidSphere(.21, 16, 16);
}

void Sphere3D::onDisplayShadow() {
}

