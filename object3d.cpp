#include "object3d.h"

int Object3D::ID_OBJECT = 0;

Object3D::Object3D() : Object3D(0, 0, 0) {}

Object3D::Object3D(GLdouble x, GLdouble y, GLdouble z) {
    this->xt = x;
    this->yt = y;
    this->zt = z;

    this->xs = 1.0;
    this->ys = 1.0;
    this->zs = 1.0;

    this->xr = 0.0;
    this->yr = 0.0;
    this->zr = 0.0;

    setSelectable(true);
    setSelected(false);

    id = ID_OBJECT++;
}

Object3D::~Object3D() {

}

void Object3D::displayCube(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth) {
    glPushMatrix();

    glTranslatef(x, y, z);
    glScaled(width/1.0, height/1.0, depth/1.0);
    glutSolidCube(1.0);

    glPopMatrix();
}

void Object3D::displayCylinder(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth) {
    glPushMatrix(); // 1

    glRotatef(90, -1, 0, 0);
    glTranslatef(x, y, z);
    glScaled(width/1.0, depth/1.0, height/1.0);

    GLUquadricObj *quad;
    quad = gluNewQuadric();
    gluCylinder(quad, 0.5, 0.5, 1, 30, 1);
    glPushMatrix(); //2
    glTranslatef(0.0f, 0.0f, 1);
    gluDisk(quad, 0.0f, 0.5, 30, 1);
    glPopMatrix(); //2
    glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
    gluDisk(quad, 0.0f, 0.5, 30, 1);
    gluDeleteQuadric(quad);
    glPopMatrix(); //1
}

void Object3D::displaySphere(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth) {
    glPushMatrix();

    glTranslatef(x, y, z);
    glScaled(width/1.0, height/1.0, depth/1.0);
    glutSolidSphere(1.0, 32, 32);

    glPopMatrix();
}

int Object3D::getId() {
    return id;
}

bool Object3D::getSelected() const {
    return selected;
}

void Object3D::setSelected(bool value) {
    selected = value;
}

bool Object3D::getSelectable() const {
    return selectable;
}

void Object3D::setSelectable(bool value) {
    selectable = value;
}

void Object3D::translate(GLdouble x, GLdouble y, GLdouble z) {
    onObjectChanged();
    this->xt += x;
    this->yt += y;
    this->zt += z;
}

void Object3D::rotate(GLdouble angle, GLdouble x, GLdouble y, GLdouble z) {

    this->xr += angle*x;
    this->yr += angle*y;
    this->zr += angle*z;
}

void Object3D::scale(GLdouble x, GLdouble y, GLdouble z) {

    this->xs += x;
    this->ys += y;
    this->zs += z;
}

void Object3D::scale(GLdouble s) {
    scale(s, s, s);
}

Vector<float> Object3D::getPosition() {
    Vector<float> pos{(float)xt, (float)yt, (float)zt};
    return pos;
}

void Object3D::display() {

    if(selectable)
        glPushName(getId());
    glPushMatrix();

    glTranslated(xt, yt, zt);
    glRotated(xr, 1, 0, 0);
    glRotated(yr, 0, 1, 0);
    glRotated(zr, 0, 0, 1);
    glScaled(xs, ys, zs);
    applyTransforms();

    if(getSelected())
        onDisplaySelected();
    else
        onDisplay();

    glPopMatrix();

    if(selectable)
        glPopName();
}

void Object3D::displaySelected() {
    glPushMatrix();

    glTranslated(xt, yt, zt);
    glRotated(xr, 1, 0, 0);
    glRotated(yr, 0, 1, 0);
    glRotated(zr, 0, 0, 1);
    glScaled(xs, ys, zs);
    applyTransforms();

    onDisplaySelected();

    glPopMatrix();
}

void Object3D::displayShadow() {
    glPushMatrix();

    glTranslated(xt, yt, zt);
    glRotated(xr, 1, 0, 0);
    glRotated(yr, 0, 1, 0);
    glRotated(zr, 0, 0, 1);
    glScaled(xs, ys, zs);
    applyTransforms();

    onDisplayShadow();

    glPopMatrix();
}

GLdouble Object3D::getX() const {
    return xt;
}

void Object3D::setX(const GLdouble &value) {

    xt = value;
}
GLdouble Object3D::getY() const {
    return yt;
}

void Object3D::setY(const GLdouble &value) {

    yt = value;
}
GLdouble Object3D::getZ() const {
    return zt;
}

void Object3D::setZ(const GLdouble &value) {

    zt = value;
}

void Object3D::setTransforms(Vector<Matrix<float> > transforms) {
    this->transforms = transforms;
}

void Object3D::onObjectChanged() {}

void Object3D::applyTransforms() {

    for(int i = 0;i < transforms.getSize(); i++)
        glMultTransposeMatrixf(transforms[i].toVector());
}






