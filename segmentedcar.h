#ifndef SEGMENTEDCAR_H
#define SEGMENTEDCAR_H

#include <car.h>
#include <vector>
#include <abscurve.h>
#include <object3d.h>

class SegmentedCar : public Object3D, public KeyListener {
public:
    SegmentedCar(AbsCurve* track, int cars = 3);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

    void onKeyCharPressed(int key);
    void onKeyPressed(int key);

    void advance();
    void goBack();

    Camera* getCamera();
    void setCamera(Camera* cam);
    void setCamera2(Camera* cam);

private:
    vector<Car*> cars;
};

#endif // SEGMENTEDCAR_H
