#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <glwidget.h>
#include <beziercurve.h>
#include <rollercoaster.h>
#include <floor3d.h>
#include <QTimer>
#include <cubiccurve.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void onAdvance();
    void onGoBack();

protected:
    void keyPressEvent(QKeyEvent *);

private slots:
    void onRun();

    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_btn_back_clicked();
    void on_sSpeed_valueChanged(int value);
    void on_sControlPoints_valueChanged(int value);
    void on_sDistance_valueChanged(int value);
    void on_sVariation_valueChanged(int value);
    void on_sbViewCount_valueChanged(int value);

    void on_radioButton_toggled(bool checked);

private:
    GLWidget* widget;
    RollerCoaster* rc;
    QTimer* timer;
    Ui::MainWindow *ui;

    int speed;
    bool toFront;

    void startTimer();
    void reshapeRollerCoaster(int a, int b, int c);

};

#endif // MAINWINDOW_H
