#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <iostream>
#include <QGLWidget>
#include <GL/glu.h>
#include <GL/glut.h>
#include <keylistener.h>
#include <object3d.h>
#include <viewport.h>
#include <QKeyEvent>
#include <QMouseEvent>

using namespace std;

class GLWidget : public QGLWidget {
public:
    GLWidget(QWidget* parent = 0);
    ~GLWidget();

    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);

    void display();
    void displayToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]);

    void addObject(Object3D* obj);
    void addObject(vector<Object3D*> objs);
    void addKeyListener(KeyListener* listener);

    void addView(ViewPort* view);
    ViewPort* getAtualView();
    void addCamToViews(Camera* cam);
    void addCamToViews();
    void transposeChoord(float &x, float &y);

    void setViewCount(int count);
    void clearObjects();
    void clearCams();
    void setPickingEnabled(bool pick);
    bool getPickingEnabled();

protected:
    void keyPressEvent(QKeyEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);

    void wheelEvent(QWheelEvent *);

private:
    GLfloat viewAngleX;
    GLfloat viewAngleY;
    GLfloat viewAngleZ;

    float mouseX, mouseY;
    bool flagMouseDownRotate;
    bool flagMouseDownMoved;
    int view;

    GLfloat lightX, lightY, lightZ;

    bool pickingEnabled;
    bool mousePressed;
    bool resizingView;
    bool moovingEnabled;
    bool rotateEnabled;
    bool scaleEnabled;
    bool keyXEnabled;
    bool keyYEnabled;
    bool keyZEnabled;

    vector<KeyListener*> listeners;
    vector<Object3D*> objects;
    vector<Camera*> cams;
    vector<ViewPort*> views;

    void nextObject(bool);
    void nextView();
    void resizeViews();
    int processHits(GLint hits, GLuint buffer[]);
    int picking( GLint cursorX, GLint cursorY, int w, int h );
    void selectObject(int id);
};

#endif // GLWIDGET_H
