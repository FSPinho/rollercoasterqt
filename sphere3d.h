#ifndef SPHERE3D_H
#define SPHERE3D_H

#include "object3d.h"

class Sphere3D : public Object3D {
public:
    Sphere3D(GLdouble x, GLdouble y, GLdouble z);
    ~Sphere3D();

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();
};

#endif // SPHERE3D_H
