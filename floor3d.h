#ifndef FLOOR3D_H
#define FLOOR3D_H

#include <object3d.h>

class Floor3D : public Object3D {
public:
    Floor3D(GLdouble x, GLdouble y, GLdouble z);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();
};

#endif // FLOOR3D_H
