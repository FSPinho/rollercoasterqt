#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    widget = ui->openGLWidget;

    // Create the roller coaster
    reshapeRollerCoaster(10, 10, 10);

    // Other curves
    Vector<ControlObjects*> c = {
        new ControlObjects(-15, 0, 0),
        new ControlObjects(-5, 10, 0),
        new ControlObjects(5, 10, 0),
        new ControlObjects(15, 0, 0)
    };

    /*
    //AbsCurve* curve = new BezierCurve(c);
    AbsCurve* curve = new CubicCurve(c);
    widget->addObject(curve);
    for(int i = 0; i < 4; i++) {
        widget->addObject(c[i]);
    }
*/
    //End

    speed = ui->sSpeed->value();
    toFront = true;

    ui->radioButton->setChecked(widget->getPickingEnabled());

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(onRun()));

}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::onAdvance() {
    rc->getCar()->advance();
    widget->updateGL();
}

void MainWindow::onGoBack() {
    rc->getCar()->goBack();
    widget->updateGL();
}

void MainWindow::keyPressEvent(QKeyEvent *e) {
    if (e->type() == QEvent::KeyPress) {
        QKeyEvent* newEvent = new QKeyEvent(QEvent::KeyPress,e->key(), e->modifiers ());
        qApp->postEvent (ui->openGLWidget, newEvent, 0);
    }
}

void MainWindow::onRun() {
    if(toFront) {
        onAdvance();
    } else {
        onGoBack();
    }
}

void MainWindow::on_pushButton_clicked() {
    toFront = true;
    startTimer();
}

void MainWindow::on_pushButton_2_clicked() {
    timer->stop();
}

void MainWindow::on_btn_back_clicked() {
    toFront = false;
    startTimer();
}

void MainWindow::on_sSpeed_valueChanged(int value) {
    speed = value;
    timer->setInterval(1000/speed);
}

void MainWindow::startTimer() {
    timer->start(1000/speed);
}

void MainWindow::reshapeRollerCoaster(int a, int b, int c) {
    cout << "Reload shape" << endl;
    widget->clearObjects();
    widget->clearCams();

    widget->addObject(new Floor3D(0, 0, 0));

    rc = new RollerCoaster(0, 10, 0, a, b, c);
    widget->addObject(rc->getObjects());
    widget->addCamToViews(rc->getCamera());
    widget->addCamToViews(rc->getCamera2());
    widget->addKeyListener(rc->getKeyListener());
}

void MainWindow::on_sControlPoints_valueChanged(int value) {
    int points = ui->sControlPoints->value();
    int distance = ui->sDistance->value();
    int variation = ui->sVariation->value();

    reshapeRollerCoaster(points, distance, variation);
    widget->updateGL();
}

void MainWindow::on_sDistance_valueChanged(int value) {
    int points = ui->sControlPoints->value();
    int distance = ui->sDistance->value();
    int variation = ui->sVariation->value();

    reshapeRollerCoaster(points, distance, variation);
    widget->updateGL();
}

void MainWindow::on_sVariation_valueChanged(int value) {
    int points = ui->sControlPoints->value();
    int distance = ui->sDistance->value();
    int variation = ui->sVariation->value();

    reshapeRollerCoaster(points, distance, variation);
    widget->updateGL();
}

void MainWindow::on_sbViewCount_valueChanged(int value) {
    widget->setViewCount(ui->sbViewCount->value());
}

void MainWindow::on_radioButton_toggled(bool checked) {
    widget->setPickingEnabled(checked);
}
