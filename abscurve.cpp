#include "abscurve.h"

AbsCurve::AbsCurve(Vector<Vector<float> > points) {
    for(int i = 0; i < points.getSize(); i++)
        controlPoints << points[i];

    M = MATRIX_CUBIC;
    numberOfSegments = 40;
    showVertexes = false;
    memorized = false;
    toUpdate = false;

    setSelectable(false);
    reloadFromObjects();
}

AbsCurve::AbsCurve(Vector<ControlObjects *> controlObjects) {
    this->controlObjects = controlObjects;

    M = MATRIX_CUBIC;
    numberOfSegments = 40;
    showVertexes = false;
    memorized = false;
    toUpdate = true;

    setSelectable(false);
}

void AbsCurve::reset(Vector<ControlObjects *> controlObjects) {
    this->controlObjects = controlObjects;

    M = MATRIX_CUBIC;
    numberOfSegments = 40;
    showVertexes = false;
    memorized = false;
    toUpdate = true;

    setSelectable(false);
}

void AbsCurve::reset(Vector<Vector<float> > points) {
    controlPoints.clear();
    for(int i = 0; i < points.getSize(); i++)
        controlPoints << points[i];

    M = MATRIX_CUBIC;
    numberOfSegments = 40;
    showVertexes = false;
    memorized = false;
    toUpdate = false;

    setSelectable(false);
    reloadFromObjects();
}

void AbsCurve::onDisplay() {
    if(toUpdate) {
        toUpdate = false;
        reloadFromObjects();
    }

    int curves = controlPoints.getHeight() - 3;

    if(isShowVertexes()) {
        glColor3f(0, 0, 0);
        for(int u = 0; u <= numberOfSegments * (curves - (curves > 1? 1: 0)); u += 1) {
            glPushMatrix();
                glMultTransposeMatrixf(getTransposeMatrix(u).toVector());
                Object3D::displayCube(0, 0, 0, .5, .1, .1);
                Object3D::displayCube(.2, 0, 0, .1, .1, .5);
                Object3D::displayCube(-.2, 0, 0, .1, .1, .5);
            glPopMatrix();
        }
    }

    glBegin(GL_LINES);
    glColor3f(0, 0, 0);
    for(int u = 0; u < (numberOfSegments * (curves - (curves > 1? 1: 0))) - 1; u += 1) {
        glPushMatrix();
            Vector<float> p1 = getPointAt(u);
            Vector<float> p2 = getPointAt(u + 1);
            glVertex3f(p1.getX(), p1.getY(), p1.getZ());
            glVertex3f(p2.getX(), p2.getY(), p2.getZ());
        glPopMatrix();
    }
    glEnd();
}

void AbsCurve::onDisplaySelected() { onDisplay(); }

void AbsCurve::onDisplayShadow() {}

void AbsCurve::notifyChanged() {
    toUpdate = true;
}

void AbsCurve::setControllPoints(Vector<float> p) {
    Matrix<float> mp;
    mp << p;
    controlPoints = mp;
}

int AbsCurve::getNumberOfSegments() {
    return numberOfSegments;
}

void AbsCurve::setNumberOfSegments(int value) {
    numberOfSegments = value;
    reloadFromObjects();
}

bool AbsCurve::isShowVertexes() const { return showVertexes; }

void AbsCurve::setShowVertexes(bool value) { showVertexes = value; }

Matrix<float> AbsCurve::getM() {
    return M;
}

void AbsCurve::setM(const Matrix<float> &value) { M = value; }

Matrix<float> AbsCurve::getControlPoints() {
    return controlPoints;
}

void AbsCurve::setControlPoints(const Matrix<float> &value) {
    controlPoints = value;
}

void AbsCurve::init() {
    reloadFromObjects();
}

void AbsCurve::precalculatePoints() {
    memorized = false;

    memoPoints.clear();
    memoDeriveds1.clear();
    memoDeriveds2.clear();

    int curves = controlPoints.getHeight() - 3;
    for(int u = 0; u <= numberOfSegments * (curves - (curves > 1? 1: 0)); u += 1) {
        memoPoints << getPointAt(u);
        memoDeriveds1 << getDerived1(u);
        memoDeriveds2 << getDerived2(u);
    }

    memorized = true;
}

Vector<float> AbsCurve::getPointAt(int part) {
    if(memorized) {
        return memoPoints[part];
    } else {
        int begin = part/getNumberOfSegments();
        float u = (float)(part % getNumberOfSegments())/(float)getNumberOfSegments();

        Matrix<float> M = getM();

        Matrix<float> ut = {{u*u*u , u*u, u, 1}};
        Matrix<float> p;
        p << getControlPoints()[begin] << getControlPoints()[begin + 1] << getControlPoints()[begin + 2] << getControlPoints()[begin + 3];

        Matrix<float> res = ut*M*p;
        return res[0];
    }
}

Vector<float> AbsCurve::getDerived1(int part) {

    if(memorized) {
        return memoDeriveds1[part];
    } else {
        int begin = part/getNumberOfSegments();
        float u = (float)(part % getNumberOfSegments())/(float)getNumberOfSegments();

        Matrix<float> M = getM();

        Matrix<float> ut = {{3.0f*u*u, 2.0f*u, 1.0f, 0.0f}};
        Matrix<float> p;
        p << getControlPoints()[begin] << getControlPoints()[begin + 1] << getControlPoints()[begin + 2] << getControlPoints()[begin + 3];

        Matrix<float> res = ut*M*p;
        return res[0];
    }
}

Vector<float> AbsCurve::getDerived2(int part) {

    if(memorized) {
        return memoDeriveds2[part];
    } else {
        int begin = part/getNumberOfSegments();
        float u = (float)(part % getNumberOfSegments())/(float)getNumberOfSegments();

        Matrix<float> M = getM();

        Matrix<float> ut = {{6.0f*u, 2.0f, 0.0f, 0.0f}};
        Matrix<float> p;
        p << getControlPoints()[begin] << getControlPoints()[begin + 1] << getControlPoints()[begin + 2] << getControlPoints()[begin + 3];

        Matrix<float> res = ut*M*p;
        return res[0];
    }
}

int AbsCurve::getNumberOfCurves() {
    int points = controlPoints.getHeight() - 3;
    int curves = (numberOfSegments * (points - (points > 1? 1: 0)));
    return curves;
}

void AbsCurve::reloadFromObjects() {
    if(controlObjects.getSize() > 0) {
        Matrix<float> aux;
        for(int i = 0; i < controlObjects.getSize(); i++) {
            controlObjects[i]->setObserver(this);
            Vector<float> v = controlObjects[i]->getPosition();
            aux << v;
        }

        controlPoints = aux;
    }

    precalculatePoints();
}

Matrix<float> AbsCurve::getTransposeMatrix(int pos) {
    Vector<float> t = getPointAt(pos);

    Vector<float> zl = getDerived1(pos)*-1;
    Vector<float> yl = getDerived2(pos);
    Vector<float> xl = yl * zl;
    yl = zl * xl;
    xl.normalize();
    yl.normalize();
    zl.normalize();

    Matrix<float> T {
        { xl.getX(), yl.getX(), zl.getX(), t.getX() },
        { xl.getY(), yl.getY(), zl.getY(), t.getY() },
        { xl.getZ(), yl.getZ(), zl.getZ(), t.getZ() },
        { 0, 0, 0, 1 }
    };

    zl = getDerived1(pos)*-1;
    yl = getDerived2(pos);

    return T;
}
