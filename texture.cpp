#include "texture.h"

Texture::Texture() {

}

void Texture::prepare() {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    gluBuild2DMipmaps( GL_TEXTURE_2D, wood_image.bytes_per_pixel, wood_image.width, wood_image.height, GL_RGB,GL_UNSIGNED_BYTE, wood_image.pixel_data );
    glEnable(GL_TEXTURE_2D);

    // Draw object...
}

void Texture::clear() {
    glDisable(GL_TEXTURE_2D);
}
