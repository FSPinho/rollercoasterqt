#-------------------------------------------------
#
# Project created by QtCreator 2015-06-24T05:17:01
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RollerCoasterQT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp \
    viewport.cpp \
    camera.cpp \
    abscurve.cpp \
    beziercurve.cpp \
    bsplinecurve.cpp \
    car.cpp \
    catmullromcurve.cpp \
    controlobjects.cpp \
    cube3d.cpp \
    cubiccurve.cpp \
    floor3d.cpp \
    object3d.cpp \
    objreader.cpp \
    observer.cpp \
    rollercoaster.cpp \
    sphere3d.cpp \
    segmentedcar.cpp \
    woodtexture.cpp \
    texture.cpp

HEADERS  += mainwindow.h \
    glwidget.h \
    viewport.h \
    camera.h \
    abscurve.h \
    beziercurve.h \
    bsplinecurve.h \
    car.h \
    catmullromcurve.h \
    controlobjects.h \
    cube3d.h \
    cubiccurve.h \
    floor3d.h \
    keylistener.h \
    matrix.h \
    object3d.h \
    objreader.h \
    observer.h \
    rollercoaster.h \
    sphere3d.h \
    vector.h \
    matrixes.h \
    segmentedcar.h \
    wood.h \
    woodtexture.h \
    texture.h

FORMS    += mainwindow.ui

LIBS += -lGLU -lglut
CONFIG += c++11

DISTFILES += \
    RollerCoasterQT.pro.user
