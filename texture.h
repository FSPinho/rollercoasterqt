#ifndef TEXTURE_H
#define TEXTURE_H

#include <wood.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

class Texture {
public:
    Texture();

    void prepare();
    void clear();

};

#endif // TEXTURE_H
