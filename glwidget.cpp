#include "glwidget.h"

GLWidget::GLWidget(QWidget* parent) : QGLWidget(parent) {
    viewAngleX = 0.0f;
    viewAngleY = 0.0f;

    mousePressed = false;
    flagMouseDownRotate = false;
    flagMouseDownMoved = false;
    mouseX = mouseY = 0;

    lightX = 50;
    lightY = 50;
    lightZ = 50;

    resizingView = false;
    moovingEnabled = false;
    rotateEnabled = false;
    scaleEnabled = false;
    keyXEnabled = false;
    keyYEnabled = false;
    keyZEnabled = false;

    int nViews = 1;
    setViewCount(nViews);
    view = 0;

    setPickingEnabled(true);
    setMouseTracking(true);
}

GLWidget::~GLWidget() {

}

void GLWidget::initializeGL() {
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);

    GLfloat luzAmbiente[4]={0.2 ,0.2 ,0.2, 1.0};
    GLfloat luzDifusa[4]={0.7, 0.7, 0.7, 1.0};
    GLfloat luzEspecular[4]={1.0, 1.0, 1.0, 1.0};
    GLfloat posicaoLuz[4]={50, 50, 50, 0.0};

    GLfloat especularidade[4]={1.0,1.0,1.0,1.0};
    GLint especMaterial = 100;

    glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
    glMateriali(GL_FRONT,GL_SHININESS, especMaterial);

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

    glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa );
    glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular );
    glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );

    glEnable(GL_CULL_FACE);
}

void GLWidget::paintGL() {
    display();
}

void GLWidget::resizeGL(int w, int h) {
    float scaleW = (float)w/width();
    float scaleH = (float)h/height();
    for(auto i = views.begin(); i != views.end(); i++)
        (*i)->reshape(scaleW, scaleH);

    for(auto i = views.begin(); i != views.end(); i++)
        (*i)->reloadViewParams();

    resizeViews();
}

void GLWidget::display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    /* Display objects */
    int aux = 0;
    for(auto i = views.begin(); i != views.end(); i++) {
        ViewPort* view = (*i);
        view->display();

        for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++) {
            (*i)->display();
            aux++;
        }
    }
    glFlush();
    //glutSwapBuffers();
}

void GLWidget::displayToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    /* Display objects */
    int aux = 0;
    for(auto i = views.begin(); i != views.end(); i++) {
        ViewPort* view = (*i);
        view->displayToSelect(cursorX, cursorY, w, h, BUFSIZE, selectBuf);

        for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++) {
            glPushName((*i)->getId());
            (*i)->display();
            glPopName();
            aux++;
        }
    }
    glFlush();
    //glutSwapBuffers();
}

void GLWidget::addObject(Object3D* obj) {
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->setSelected(false);
    this->objects.push_back(obj);

    if(obj->getSelectable())
        obj->setSelected(true);
    cout << "Add object: " << obj->getId() << endl;
}

void GLWidget::addObject(vector<Object3D *> objs) {
    for(vector<Object3D*>::iterator i = objs.begin(); i != objs.end(); i++) {
        (*i)->setSelected(false);
        addObject(*i);
    }
}

void GLWidget::addKeyListener(KeyListener* listener) {
    listeners.push_back(listener);
}

void GLWidget::addView(ViewPort *view) {
    views.insert(views.begin(), view);

    resizeViews();
}

void GLWidget::nextObject(bool next) {

    bool containsSelectable = false;
    bool selected = false;
    int size = objects.size();
    for(int i = 0; i < size; i++) {
        containsSelectable |= objects.at(i)->getSelectable();
        selected |= objects.at(i)->getSelected();
    }

    if(!selected)
        objects.at(0)->setSelected(true);

    if(objects.size() > 0) {
        int size = objects.size();
        for(int i = 0; i < size; i++) {
            Object3D* object = objects.at(i);
            if(object->getSelected()) {

                object->setSelected(false);

                if(next)
                    object = objects.at((i + 1) % size);
                else
                    object = objects.at((i + (size - 1)) % size);

                if(containsSelectable) {
                    object->setSelected(true);

                    if(!object->getSelectable())
                        nextObject(next);
                }

                break;
            }
        }
    }
}

void GLWidget::nextView() {
    ++view %= views.size();
    for(auto v: views)
        v->setSelected(false);
    getAtualView()->setSelected(true);
}

void GLWidget::resizeViews() {
    int columns = ceil( sqrt(views.size()) );
    int lines = views.size()/columns;
    float colWidth = (float)width()/(columns);
    float lineHeight = (float)height()/((unsigned int)columns*lines == views.size()? lines: lines + 1);

    for(int i = 0; i < lines; i++) {
        for(int j = 0; j < columns; j++) {
            float x = j*colWidth;
            float y = i*lineHeight;

            ViewPort* view = views[i*columns + j];
            view->reshape(x, y, colWidth, lineHeight);
        }
    }

    int numIncorrectViews = views.size() -(columns*lines);
    int numCorrectViews = views.size() - numIncorrectViews;

    if(numIncorrectViews > 0) {
        colWidth = width()/(views.size() - numCorrectViews);
        for(unsigned int i = numCorrectViews; i < views.size(); i++) {
            float x = (i - numCorrectViews)*colWidth;
            float y = (lines)*lineHeight;

            ViewPort* view = views[i];
            view->reshape(x, y, colWidth, lineHeight);
        }
    }
}

int GLWidget::processHits(GLint hits, GLuint buffer[]) {
    int i;
    GLuint names, *ptr, minZ,*ptrNames, numberOfNames;

    ptrNames = NULL;

    printf("Hits = %d\n",hits);
    printf("Buffer = ");
    for (i = 0; i < 4*hits; i++) {
        printf("%u ",buffer[i]);
    }
    printf("\n");

    ptr = (GLuint *) buffer;
    minZ = 0xffffffff;
    for (i = 0; i < hits; i++) {
        names = *ptr;
        ptr++;

        if (*ptr < minZ) {
            numberOfNames = names;
            minZ = *ptr;
            if (numberOfNames != 0)
            ptrNames = ptr+2;
        }
        ptr += names+2;
    }

    if (ptrNames == NULL)
    return 0;
    else
        return *ptrNames;
}

int GLWidget::picking(GLint cursorX, GLint cursorY, int w, int h) {
    int BUFSIZE= 512;
    GLuint selectBuf[BUFSIZE];
    displayToSelect(cursorX, cursorY, w, h, BUFSIZE, selectBuf);

    int hits;

    hits = glRenderMode(GL_RENDER);

    if (hits != 0) {
        return processHits(hits,selectBuf);
    } else {
        return 0;
    }
}

void GLWidget::selectObject(int id) {
    Object3D* obj = NULL;
    for(auto i = objects.begin(); i != objects.end(); i++) {
        Object3D* o = (*i);
        o->setSelected(false);
        if(o->getId() == id && o->getSelectable()) {
            obj = o;
        }
    }

    if(obj != NULL) {
        cout << "Selecting: " << id << endl;
        obj->setSelected(true);
    }
}

ViewPort *GLWidget::getAtualView() {
    return views.at(view);
}

void GLWidget::addCamToViews(Camera *cam) {
    this->cams.push_back(cam);
    for(auto i = views.begin(); i != views.end(); i++) {
        ViewPort* view = (*i);
        view->addCamera(cam);
    }
}

void GLWidget::addCamToViews() {
    for(auto cam: cams) {
        for(auto i = views.begin(); i != views.end(); i++) {
            ViewPort* view = (*i);
            view->addCamera(cam);
        }
    }
}

void GLWidget::transposeChoord(float &x, float &y) {
    y = height() - y;
}

void GLWidget::setViewCount(int count) {
    count = count > 0? count: 1;
    view = 0;
    views.clear();
    for(int i = 0; i < count; i++) {
        ViewPort* view = new ViewPort();
        addView(view);
    }

    addCamToViews();
    resizeViews();
    updateGL();
}

void GLWidget::clearObjects() {
    objects.clear();
}

void GLWidget::clearCams() {
    for(auto v: views)
        v->clearCams();
}

void GLWidget::setPickingEnabled(bool pick) {
    this->pickingEnabled = pick;
}

bool GLWidget::getPickingEnabled() {
    return pickingEnabled;
}

void GLWidget::keyPressEvent(QKeyEvent *e) {
    cout << e->text().toStdString() << endl;
    int keyCode = e->key();

    switch(keyCode) {
    case Qt::Key_Up:
        nextObject(true);
        break;
    case Qt::Key_Down:
        nextObject(false);
        break;
    case Qt::Key_Right:
        nextObject(true);
        break;
    case Qt::Key_Left:
        nextObject(false);
        break;
    }

    for(auto i = listeners.begin(); i != listeners.end(); i++) {
        (*i)->onKeyPressed(keyCode);
    }

    char keyChar = e->text().size() > 0? e->text().toStdString().at(0): 0;
    switch(keyCode) {
    case Qt::Key_X:
        cout << "x" << endl;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled= false;
        break;
    case Qt::Key_Y:
        cout << "y" << endl;
        keyYEnabled = true;
        keyXEnabled = keyZEnabled= false;
        break;
    case Qt::Key_Z:
        cout << "z" << endl;
        keyZEnabled = true;
        keyXEnabled = keyYEnabled= false;
        break;
    case Qt::Key_G:
        cout << "Translate" << endl;
        moovingEnabled = true;
        rotateEnabled = false;
        scaleEnabled = false;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case Qt::Key_R:
        cout << "Rotate" << endl;
        moovingEnabled = false;
        rotateEnabled = true;
        scaleEnabled = false;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case Qt::Key_S:
        cout << "Scale" << endl;
        moovingEnabled = false;
        rotateEnabled = false;
        scaleEnabled = true;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case Qt::Key_Escape:
        cout << "View" << endl;
        moovingEnabled = false;
        scaleEnabled = false;
        rotateEnabled = false;
        break;
    case Qt::Key_C:
        cout << "Change Cam" << endl;
        getAtualView()->nextCamera();
        break;
    case Qt::Key_V:
        cout << "Change View" << endl;
        nextView();
        break;

    }

    for(auto i = listeners.begin(); i != listeners.end(); i++) {
        (*i)->onKeyCharPressed(keyCode);
    }

    // Rever
    updateGL();
}

void GLWidget::mousePressEvent(QMouseEvent *e) {
    setFocus();
    mousePressed = true;

    int button = e->button();
    int xi = e->x(), yi = e->y();
    float x = xi, y = yi;
    transposeChoord(x, y);

    if (button == Qt::LeftButton && pickingEnabled) {
        int pick = picking(xi, yi, 5, 5);
        selectObject(pick);
    }

    if (button == Qt::LeftButton) {

        for(unsigned int i = 0; i < views.size(); i++) {
            if(views[i]->containsClick(x, y)) {
                views[i]->setSelected(true);
                view = i;
            } else {
                views[i]->setSelected(false);
            }
        }


        flagMouseDownRotate = true;
        mouseX = x; mouseY = y;
        //flagMouseDownRotate = false;
    }

    if (button == Qt::RightButton) {
        flagMouseDownMoved = true;
        mouseX = x; mouseY = y;
        //flagMouseDownMoved = false;
    }

    updateGL();
}

void GLWidget::mouseReleaseEvent(QMouseEvent *) {
    mousePressed = false;
    flagMouseDownRotate = false;
    flagMouseDownMoved = false;
    setCursor(Qt::ArrowCursor);
}

void GLWidget::mouseMoveEvent(QMouseEvent *e) {
    int xi = e->x(), yi = e->y();
    float x = xi, y = yi;
    transposeChoord(x, y);

    Object3D* object = NULL;
    for(unsigned int i = 0; i < objects.size(); i++) {
        if(objects.at(i)->getSelected()) {
            object = objects.at(i);
        }
    }

    if(mousePressed) {
        if(resizingView) {
            bool movable = true;
            for(auto i = views.begin(); i != views.end(); i++) {
                ViewPort* view = (*i);
                if(view->isMarked())
                    movable &= view->isMovableBound(-(mouseX - x), -(mouseY - y));
            }

            if(movable) {
                for(auto i = views.begin(); i != views.end(); i++) {
                    ViewPort* view = (*i);
                    view->moveBound(-(mouseX - x), -(mouseY - y));
                }
            }

        } else if(moovingEnabled) {
            if(keyXEnabled && object != NULL) {
                object->translate(-(mouseX - x)*0.1, 0, 0);

            } else if(keyYEnabled && object != NULL) {
                object->translate(0, (mouseY - y)*0.1, 0);

            } else if(keyZEnabled && object != NULL) {
                object->translate(0, 0, -(mouseY - y)*0.1);

            }
        } else if(rotateEnabled) {
            if(keyXEnabled && object != NULL) {
                object->rotate(-(mouseY - y)*0.5, 1, 0, 0);

            } else if(keyYEnabled && object != NULL) {
                object->rotate(-(mouseX - x)*0.5, 0, 1, 0);

            } else if(keyZEnabled && object != NULL) {
                object->rotate((mouseY - y)*0.5, 0, 0, 1);

            }
        } else if(scaleEnabled) {
            if(keyXEnabled && object != NULL) {
                object->scale((mouseY - y)*0.1);
            }

        } else if(flagMouseDownRotate) {
            setCursor(Qt::ArrowCursor);
            viewAngleY = (mouseX - x)*0.01;
            viewAngleX = -(mouseY - y)*0.01;

            getAtualView()->setAngles(viewAngleY, viewAngleX);

        } else if(flagMouseDownMoved) {
            setCursor(Qt::ClosedHandCursor);
            float mx = (mouseX - x)*1.1;
            float my = -(mouseY - y)*1.1;
            getAtualView()->moveCam(mx, my);
        }

        mouseX = x;
        mouseY = y;

    } else {
        // Passive

        bool vertical = false;
        bool horizontal = false;
        for(auto i = views.begin(); i != views.end(); i++) {
            ViewPort* view = (*i);

            view->markBound(x, y);

            if(view->inTop(x, y) || view->inBottom(x, y)) {
                horizontal = true;
            }
            if(view->inLeft(x, y) || view->inRight(x, y)) {
                vertical = true;
            }
        }

        resizingView = horizontal || vertical;

        if(vertical && horizontal)
            setCursor(Qt::SizeAllCursor);
        else if(horizontal)
            setCursor(Qt::SplitVCursor);
        else if(vertical)
            setCursor(Qt::SplitHCursor);
        else
            setCursor(Qt::ArrowCursor);

        mouseX = x;
        mouseY = y;
    }

    // Rever
    updateGL();
}

void GLWidget::wheelEvent(QWheelEvent * e) {
    if(e->angleDelta().y() > 0) {
        getAtualView()->zoom(0.9f);
    } else {
        getAtualView()->zoom(1.1f);
    }

    updateGL();
}


