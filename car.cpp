#include "car.h"

Car::Car(AbsCurve* track) : Object3D(0, 0, 0) {
    this->track = track;
    trackWidth = track->getNumberOfCurves();
    pos = track->getNumberOfSegments()/4;
    this->cam = NULL;
    this->cam2 = NULL;

    setExtraSpaces(0, 0);

    setSelectable(false);
    updatePosition();
}

void Car::setTrack(AbsCurve *track) {
    this->track = track;
    trackWidth = track->getNumberOfCurves();
    pos = track->getNumberOfSegments()/4;
    this->cam = NULL;
    this->cam2 = NULL;

    setSelectable(false);
    updatePosition();
}

void Car::onDisplay() {
    glColor3f(1, 1, 1);
    glScalef(0.5f, 0.5f, 1.0f);

    Texture t;
    t.prepare();
    solidCube(1);
    t.clear();
}

void Car::onDisplaySelected() {
    onDisplay();
}

void Car::onDisplayShadow() {

}

void Car::onKeyPressed(int key) {}

void Car::onKeyCharPressed(int key) {
    switch(key) {
        case Qt::Key_A:
            goBack();
            break;
        case Qt::Key_D:
            advance();
            break;
    }
}

void Car::advance() {
    pos += pos < trackWidth - extraSpaceFront? 1: 0;
    updatePosition();
}

void Car::goBack() {
    pos -= pos > track->getNumberOfSegments()/4 + extraSpaceBack? 1: 0;
    updatePosition();
}

Matrix<float> Car::getTransposeMatrix(int pos) {
    Vector<float> t = track->getPointAt(pos);

    Vector<float> zl = track->getDerived1(pos)*-1;
    Vector<float> yl = track->getDerived2(pos);
    Vector<float> xl = yl * zl;
    yl = zl * xl;
    xl.normalize();
    yl.normalize();
    zl.normalize();

    Matrix<float> T {
        { xl.getX(), yl.getX(), zl.getX(), t.getX() },
        { xl.getY(), yl.getY(), zl.getY(), t.getY() },
        { xl.getZ(), yl.getZ(), zl.getZ(), t.getZ() },
        { 0, 0, 0, 1 }
    };

    zl = track->getDerived1(pos)*-1;
    yl = track->getDerived2(pos);

    return T;
}

Camera *Car::getCamera() {
    return cam;
}

void Car::setCamera(Camera* cam) {
    this->cam = cam;
    updatePosition();
}

void Car::setCamera2(Camera *cam) {
    this->cam2 = cam;
    updatePosition();
}

void Car::setExtraSpaces(int back, int front) {
    this->extraSpaceBack = back;
    this->extraSpaceFront = front;

    if(pos < track->getNumberOfSegments()/4 + back)
        pos = track->getNumberOfSegments()/4 + back;
    else if(pos > trackWidth - front)
        pos = trackWidth - front;
}

void Car::updatePosition() {

    trackWidth = track->getNumberOfCurves();
    Vector<float> point = track->getPointAt(pos);
    Matrix<float> position {
        {1, 0, 0, point.getX()},
        {0, 1, 0, point.getY()},
        {0, 0, 1, point.getZ()},
        {0, 0, 0, 0}
    };

    Matrix<float> toUp1 {
        {1, 0, 0, 0},
        {0, 1, 0, 1},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    Matrix<float> toUp2 {
        {1, 0, 0, 0},
        {0, 1, 0, 2},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    Vector<float> point2 = track->getPointAt(pos + 5);
    Matrix<float> toFront {
        {1, 0, 0, point2.getX()},
        {0, 1, 0, point2.getY()},
        {0, 0, 1, point2.getZ()},
        {0, 0, 0, 1}
    };

    Matrix<float> toBack {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 2},
        {0, 0, 0, 1}
    };

    Matrix<float> toRight {
        {1, 0, 0, 0},
        {0, 1, 0, 40},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    Vector<float> zl = track->getDerived1(pos)*-1;
    Vector<float> yl = track->getDerived2(pos);
    Vector<float> xl = yl * zl;
    yl = zl * xl;
    yl.normalize();

    setTransforms({getTransposeMatrix(pos), toUp1});

    if(cam != NULL) {
        cam->setTransforms({getTransposeMatrix(pos), toBack, toUp1});
        cam->setEyePosition({toUp2, getTransposeMatrix(pos - track->getNumberOfSegments()/4)});
        cam->setCenterPosition({toUp2, getTransposeMatrix(pos)});
        cam->setUpPosition(yl.getX(), yl.getY(), yl.getZ());
    }
    if(cam2 != NULL) {
        cam2->setTransforms({getTransposeMatrix(pos), toBack, toUp1});
        cam2->setEyePosition({toRight, toUp2, getTransposeMatrix(pos >= track->getNumberOfSegments()/4? pos - track->getNumberOfSegments()/4: pos)});
        cam2->setCenterPosition({toUp2, getTransposeMatrix(pos)});
        cam2->setUpPosition(yl.getX(), yl.getY(), yl.getZ());
    }
}

void Car::drawBox(GLfloat size, GLenum type) {
    static GLfloat n[6][3] = {
        {-1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {1.0, 0.0, 0.0},
        {0.0, -1.0, 0.0},
        {0.0, 0.0, 1.0},
        {0.0, 0.0, -1.0}
    };
    static GLint faces[6][4] = {
        {0, 1, 2, 3},
        {3, 2, 6, 7},
        {7, 6, 5, 4},
        {4, 5, 1, 0},
        {5, 6, 2, 1},
        {7, 4, 0, 3}
    };
    GLfloat v[8][3];
    GLint i;

    v[0][0] = v[1][0] = v[2][0] = v[3][0] = -size / 2;
    v[4][0] = v[5][0] = v[6][0] = v[7][0] = size / 2;
    v[0][1] = v[1][1] = v[4][1] = v[5][1] = -size / 2;
    v[2][1] = v[3][1] = v[6][1] = v[7][1] = size / 2;
    v[0][2] = v[3][2] = v[4][2] = v[7][2] = -size / 2;
    v[1][2] = v[2][2] = v[5][2] = v[6][2] = size / 2;

    for (i = 5; i >= 0; i--) {
        glBegin(type);
        glNormal3fv(&n[i][0]);
        glTexCoord2d(0, 0);
        glVertex3fv(&v[faces[i][0]][0]);
        glTexCoord2d(1, 0);
        glVertex3fv(&v[faces[i][1]][0]);
        glTexCoord2d(1, 1);
        glVertex3fv(&v[faces[i][2]][0]);
        glTexCoord2d(0, 1);
        glVertex3fv(&v[faces[i][3]][0]);
        glEnd();
    }
}

void Car::solidCube(GLdouble size) {
    drawBox(size, GL_QUADS);
}
