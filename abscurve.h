#ifndef ABSCURVE_H
#define ABSCURVE_H

#include "matrix.h"
#include "object3d.h"
#include "matrixes.h"
#include "observer.h"
#include "controlobjects.h"

class AbsCurve : public Object3D, public Observer {
public:
    AbsCurve(Vector< Vector<float> > controlPoints);
    AbsCurve(Vector< ControlObjects* > controlObjects);

    void reset(Vector< ControlObjects* > controlObjects);
    void reset(Vector< Vector<float> > controlPoints);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

    void notifyChanged();

    void setControllPoints(Vector<float> p);

    virtual Vector<float> getPointAt(int u);
    virtual Vector<float> getDerived1(int u);
    virtual Vector<float> getDerived2(int u);

    int getNumberOfCurves();

    int getNumberOfSegments();
    void setNumberOfSegments(int value);

    bool isShowVertexes() const;
    void setShowVertexes(bool value);

    Matrix<float> getM();
    void setM(const Matrix<float> &value);

    Matrix<float> getControlPoints();
    void setControlPoints(const Matrix<float> &value);


    void init();

private:
    int numberOfSegments;
    bool showVertexes;
    bool memorized;
    bool toUpdate;

    Matrix<float> M;
    Matrix<float> controlPoints;
    Matrix<float> memoPoints;
    Matrix<float> memoDeriveds1;
    Matrix<float> memoDeriveds2;

    Vector<ControlObjects*> controlObjects;

    void precalculatePoints();
    void reloadFromObjects();
    Matrix<float> getTransposeMatrix(int pos);
};

#endif // ABSCURVE_H
