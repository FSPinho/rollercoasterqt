#include "controlobjects.h"

ControlObjects::ControlObjects(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {
    observer = NULL;
}

void ControlObjects::onDisplay() {
    glColor3f(0, 0, 1);
    glutSolidCube(1.0);
}

void ControlObjects::onDisplaySelected() {
    glColor3f(1, 0, 0);
    glutSolidCube(1.0);
}

void ControlObjects::onDisplayShadow() {

}

void ControlObjects::onObjectChanged() {
    if(observer != NULL) {
        observer->notifyChanged();
    }
}

Observer *ControlObjects::getObserver() const {
    return observer;
}

void ControlObjects::setObserver(Observer *value) {
    observer = value;
}


