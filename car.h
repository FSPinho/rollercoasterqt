#ifndef CAR_H
#define CAR_H

#include "object3d.h"
#include "abscurve.h"
#include "keylistener.h"
#include "camera.h"
#include "texture.h"

#include <QObject>

class Car : public Object3D, public KeyListener {
public:
    Car(AbsCurve* track);

    void setTrack(AbsCurve* track);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

    void onKeyPressed(int key);
    void onKeyCharPressed(int key);

    void advance();
    void goBack();
    Matrix<float> getTransposeMatrix(int pos);

    Camera* getCamera();
    void setCamera(Camera* cam);
    void setCamera2(Camera* cam);

    void setExtraSpaces(int back, int front);

private:
    AbsCurve* track;
    Camera* cam;
    Camera* cam2;
    int pos;
    int trackWidth;

    // Gambi :)
    int extraSpaceFront;
    int extraSpaceBack;

    void updatePosition();
    void drawBox(GLfloat size, GLenum type);
    void solidCube(GLdouble size);
};

#endif // CAR_H
