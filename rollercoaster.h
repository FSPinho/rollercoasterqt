#ifndef ROLLERCOASTER_H
#define ROLLERCOASTER_H

#include "catmullromcurve.h"
#include "controlobjects.h"
#include "bsplinecurve.h"
#include "keylistener.h"
#include "objreader.h"
#include "sphere3d.h"
#include "camera.h"
#include "segmentedcar.h"

#include <vector>
#include <string>

using namespace std;

class RollerCoaster : public Object3D {
public:
    RollerCoaster(GLdouble x, GLdouble y, GLdouble z, int numberOfControlPoints, float distanceBetweenPoints, float variation);
    RollerCoaster(GLdouble x, GLdouble y, GLdouble z);

    KeyListener* getKeyListener();
    Camera* getCamera();
    Camera* getCamera2();
    SegmentedCar* getCar();

    vector<Object3D*> getObjects();

    void setPrecision(int prec);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

private:
    Vector<ControlObjects*> controllObjects;
    AbsCurve* track;
    Camera* cam;
    Camera* cam2;
    SegmentedCar* car;

    int numberOfControlPoints;
    float distanceBetweenPoints;
    float variation;

    void generateControllObjects();

};

#endif // ROLLERCOASTER_H
