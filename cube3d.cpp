#include "cube3d.h"

Cube3D::Cube3D() {}

Cube3D::Cube3D(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {}




void Cube3D::onDisplay() {
    glColor3f(1, 1, 1);
    glutSolidCube(1.0);
}

void Cube3D::onDisplaySelected() {
    glColor3f(1, 0, 0);
    glutSolidCube(1.0);
}

void Cube3D::onDisplayShadow() {

}
