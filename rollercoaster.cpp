#include "rollercoaster.h"

RollerCoaster::RollerCoaster(GLdouble x, GLdouble y, GLdouble z) : RollerCoaster(x, y, z, 20, 2.0f, 2.0) {}

RollerCoaster::RollerCoaster(GLdouble x, GLdouble y, GLdouble z, int numberOfControlPonts, float distanceBetweenPoints, float variation) : Object3D(x, y, z) {
    this->numberOfControlPoints = numberOfControlPonts;
    this->distanceBetweenPoints = distanceBetweenPoints;
    this->variation = variation;

    generateControllObjects();
    track = new BSplineCurve(controllObjects);
    //track = new CatmullRomCurve(controllObjects);
    track->init();
    track->setShowVertexes(true);

    car = new SegmentedCar(track);

    cam = new Camera(0, 0, 10, 0, 0, 0, 0, 1, 0);
    cam2 = new Camera(0, 0, 10, 0, 0, 0, 0, 1, 0);
    car->setCamera(cam);
    car->setCamera2(cam2);

    setSelectable(false);
}

KeyListener *RollerCoaster::getKeyListener() {
    return car;
}

Camera *RollerCoaster::getCamera() {
    return cam;
}

Camera *RollerCoaster::getCamera2() {
    return cam2;
}

SegmentedCar *RollerCoaster::getCar() {
    return car;
}

vector<Object3D *> RollerCoaster::getObjects() {
    vector<Object3D*> objects;
    objects.push_back(car);
    objects.push_back(track);

    for(int i = 0; i < controllObjects.getSize(); i++)
        objects.push_back(controllObjects[i]);

    return objects;
}

void RollerCoaster::setPrecision(int prec) {
    track->setNumberOfSegments(prec);
}

void RollerCoaster::onDisplay() {}

void RollerCoaster::onDisplaySelected() {}

void RollerCoaster::onDisplayShadow() {}

void RollerCoaster::generateControllObjects() {
    controllObjects.clear();
    float begin = -distanceBetweenPoints*numberOfControlPoints/2;
    for(int i = 0; i < numberOfControlPoints; i++) {
        float x = getX() + begin + (distanceBetweenPoints*i);
        float y = getY() + (float(rand() % (100)) / 100.0)*variation - variation/2;
        float z = getZ() + (float(rand() % (100)) / 100.0)*variation - variation/2;
        ControlObjects* controll = new ControlObjects(x, y, z);
        controllObjects << controll;
    }
}



