#include "camera.h"

Camera::Camera(GLdouble ex, GLdouble ey, GLdouble ez,\
               GLdouble cx, GLdouble cy, GLdouble cz,\
               GLdouble ux, GLdouble uy, GLdouble uz) : Object3D(0, 0, 0) {

    setEyePosition(ex, ey, ez);
    setCenterPosition(cx, cy, cz);
    setUpPosition(ux, uy, uz);

    setSelectable(false);

    top = left = -10;
    bottom = right = 10;

    setViewMode(MODE_PESPECTIVE, POINT_DEFAULT);
    fAspect = 1.0f;
    angle = 45;
}

void Camera::onDisplay() {
    glRotatef(90, 0, 1, 0);
    glPushMatrix();
        glColor3f(0.0f, 1.0f, 0.0f);
        glRotatef(-90, 0, 1, 0);
        glutSolidCone(0.3f, 1.0, 16, 16);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-1, 0, 0);
        glScalef(1, .7, .7);
        glutSolidCube(1);
    glPopMatrix();
}

void Camera::onDisplaySelected() {
    onDisplay();
}

void Camera::onDisplayShadow() {
}

void Camera::setViewMode(int mode, int point) {
    viewMode= mode;
    switch(point) {
    case POINT_DEFAULT:
        setCenterPosition(0, 0, 0);
        setEyePosition(100, 100, 100);
        break;
    case POINT_TOP:
        setCenterPosition(0, 0, 0);
        setEyePosition(0, 100, 0);
        break;
    case POINT_DOWN:
        setCenterPosition(0, 0, 0);
        setEyePosition(0, -100, 0);
        break;
    case POINT_FRONT:
        setCenterPosition(0, 0, 0);
        setEyePosition(0, 0, 100);
        break;
    case POINT_BACK:
        setCenterPosition(0, 0, 0);
        setEyePosition(0, 0, -100);
        break;
    case POINT_LEFT:
        setCenterPosition(0, 0, 0);
        setEyePosition(100, 0, 0);
        break;
    case POINT_RIGHT:
        setCenterPosition(0, 0, 0);
        setEyePosition(-100, 0, 0);
        break;
    }

}

void Camera::updateViewMode() {
    switch(viewMode) {
    case MODE_PESPECTIVE:
        gluPerspective(angle, fAspect, 0.4, 500);
        break;
    case MODE_ORTOGRPHIC:
        glOrtho(left, right, top, bottom, 0, 1000);
        break;
    }
}

void Camera::setAspect(float aspect) {
    this->fAspect = aspect;
}

void Camera::rotate(float theta, float x, float y, float z) {

    // Rotate x
    float angle = x*theta;
    Matrix<float> mx {
        {1, 0, 0, 0},
        {0, cos(angle), -sin(angle), 0},
        {0, sin(angle), cos(angle), 0},
        {0, 0, 0, 1}
    };

    Matrix<float> pos = getMatrixEyePosition();
    Matrix<float> resx = mx * pos;
    setEyePosition(resx[0][0], resx[1][0], resx[2][0]);


    // Rotate y
    angle = y*theta;
    Matrix<float> my {
        {cos(angle), 0, sin(angle), 0},
        {0, 1, 0, 0},
        {-sin(angle), 0, cos(angle), 0},
        {0, 0, 0, 1}
    };

    pos = getMatrixEyePosition();
    Matrix<float> resy = my * pos;
    setEyePosition(resy[0][0], resy[1][0], resy[2][0]);

    // Rotate z
    angle = z*theta;
    Matrix<float> mz {
        {cos(angle), -sin(angle), 0, 0},
        {sin(angle), cos(angle), 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    pos = getMatrixEyePosition();
    Matrix<float> resz = mz * pos;
    setEyePosition(resz[0][0], resz[1][0], resz[2][0]);

}

void Camera::rotateVertical(float angle) {
    if(abs(eyeZ) > abs(eyeX)) {

        if(eyeZ > 0) {
            rotate(angle, 1, 0, 0);
        } else {
            rotate(-angle, 1, 0, 0);
        }
    } else {

        if(eyeX > 0) {
            rotate(-angle, 0, 0, 1);
        } else {
            rotate(angle, 0, 0, 1);
        }
    }
}

void Camera::rotateHorizontal(float angle) {
    rotate(angle, 0, 1, 0);
}

void Camera::zoom(float intensity) {

    if(viewMode == MODE_PESPECTIVE) {
        Vector<float> pos{getEyeX(), getEyeY(), getEyeZ()};
        pos = pos*intensity;
        setEyePosition(pos.getX(), pos.getY(), pos.getZ());
    } else {
        top *= intensity;
        bottom *= intensity;
        left*= intensity;
        right*= intensity;
    }
}

void Camera::setEyePosition(float x, float y, float z) {
    setEyeX(x);
    setEyeY(y);
    setEyeZ(z);
}

void Camera::setEyePosition(Vector< Matrix<float> > transforms) {
    setEyePosition(0, 0, 0);
    for(int i = 0; i < transforms.getSize(); i++) {
        Matrix<float> transform = transforms[i];
        Matrix<float> pos = getMatrixEyePosition();
        Matrix<float> res = transform * pos;

        setEyePosition(res[0][0], res[1][0], res[2][0]);
    }
}

void Camera::setCenterPosition(float x, float y, float z)  {
    setCenterX(x);
    setCenterY(y);
    setCenterZ(z);
}

void Camera::setCenterPosition(Vector<Matrix<float> > transforms) {
    setCenterPosition(0, 0, 0);
    for(int i = 0; i < transforms.getSize(); i++) {
        Matrix<float> transform = transforms[i];
        Matrix<float> pos = getMatrixCenterPosition();
        Matrix<float> res = transform * pos;

        setCenterPosition(res[0][0], res[1][0], res[2][0]);
    }
}

void Camera::setUpPosition(float x, float y, float z) {
    setUpX(x);
    setUpY(y);
    setUpZ(z);
}

void Camera::setUpPosition(Vector<Matrix<float> > transforms) {
    setUpPosition(0, 0, 0);
    for(int i = 0; i < transforms.getSize(); i++) {
        Matrix<float> transform = transforms[i];
        Matrix<float> pos = getMatrixUpPosition();
        Matrix<float> res = transform * pos;

        setUpPosition(res[0][0], res[1][0], res[2][0]);
    }
}

float Camera::getEyeX() const {
    return eyeX;
}

void Camera::setEyeX(float value) {
    eyeX = value;
}

float Camera::getEyeY() const {
    return eyeY;
}

void Camera::setEyeY(float value) {
    eyeY = value;
}

float Camera::getEyeZ() const {
    return eyeZ;
}

void Camera::setEyeZ(float value) {
    eyeZ = value;
}

float Camera::getCenterX() const {
    return centerX;
}

void Camera::setCenterX(float value) {
    centerX = value;
}

float Camera::getCenterY() const {
    return centerY;
}

void Camera::setCenterY(float value) {
    centerY = value;
}

float Camera::getCenterZ() const {
    return centerZ;
}

void Camera::setCenterZ(float value) {
    centerZ = value;
}

float Camera::getUpX() const {
    return upX;
}

void Camera::setUpX(float value) {
    upX = value;
}

float Camera::getUpY() const {
    return upY;
}

void Camera::setUpY(float value) {
    upY = value;
}

float Camera::getUpZ() const {
    return upZ;
}

void Camera::setUpZ(float value) {
    upZ = value;
}

Matrix<float> Camera::getMatrixEyePosition() {
    Matrix<float> pos = {
        {getEyeX()},
        {getEyeY()},
        {getEyeZ()},
        {1}
    };

    return pos;
}

Matrix<float> Camera::getMatrixCenterPosition() {
    Matrix<float> pos = {
        {getCenterX()},
        {getCenterY()},
        {getCenterZ()},
        {1}
    };

    return pos;
}

Matrix<float> Camera::getMatrixUpPosition() {
    Matrix<float> pos = {
        {getUpX()},
        {getUpY()},
        {getUpZ()},
        {1}
    };

    return pos;
}




