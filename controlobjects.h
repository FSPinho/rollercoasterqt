#ifndef CONTROLOBJECTS_H
#define CONTROLOBJECTS_H

#include "object3d.h"
#include "observer.h"

class ControlObjects : public Object3D {
public:
    ControlObjects(GLdouble x, GLdouble y, GLdouble z);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

    Observer *getObserver() const;
    void setObserver(Observer *value);

protected:
    void onObjectChanged();

private:
    Observer* observer;
};

#endif // CONTROLOBJECTS_H
