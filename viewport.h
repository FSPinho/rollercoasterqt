#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <GL/gl.h>
#include <GL/glut.h>
#include <vector>
#include "camera.h"

#define MOUSE_LIMIT 10

using namespace std;

class ViewPort {
public:
    ViewPort();
    ViewPort(int viewMode, int point);

    void display();
    void displayToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]);
    void reshape(float scaleW, float scaleH);
    void reshape(float x, float y, float w, float h);
    void reloadViewParams();
    void reloadViewParamsToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]);

    void addCamera(Camera* cam);
    void nextCamera();
    void moveCam(float x, float y);

    void zoom(float zoom);
    void setAngles(float h, float v);
    bool containsClick(int x, int y);

    bool inBound(float x, float y);
    bool inTop(float x, float y);
    bool inBottom(float x, float y);
    bool inLeft(float x, float y);
    bool inRight(float x, float y);

    bool inBoundParalel(float x, float y);
    bool inTopParalel(float x, float y);
    bool inBottomParalel(float x, float y);
    bool inLeftParalel(float x, float y);
    bool inRightParalel(float x, float y);

    void markBound(float x, float y);
    void unmarkBound();
    bool isMarked();
    void moveBound(float x, float y);
    bool isMovableBound(float x, float y);

    float getX() const;
    void setX(float value);

    float getY() const;
    void setY(float value);

    float getWidth() const;
    void setWidth(float value);

    float getHeight() const;
    void setHeight(float value);

    Camera* getAtualCam();
    void clearCams();

    bool getSelected() const;
    void setSelected(bool value);

private:
    float x, y;
    float width, height;
    int camera;

    GLfloat viewAngleX;
    GLfloat viewAngleY;

    Camera* defaultCam;
    vector<Camera*> cams;

    bool selected;
    bool top;
    bool bottom;
    bool left;
    bool right;

};

#endif // VIEWPORT_H
